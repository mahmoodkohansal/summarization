FROM python:3

ADD index.py /

RUN pip install numpy

CMD [ "python", "./index.py" ]
